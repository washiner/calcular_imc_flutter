import 'package:flutter/material.dart';
import 'package:imc_aula/constants/app_constantes.dart';

class RigthBar extends StatelessWidget {

  final double barWidth;

  const RigthBar({Key? key, required this.barWidth}) : super(key :key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 25,
          width: barWidth,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                topRight: Radius.circular(20)
            ),
            color: accentHexacolor,
          ),
        )
      ],
    );
  }
}