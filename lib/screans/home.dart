import 'package:imc_aula/constants/app_constantes.dart';
import 'package:imc_aula/widgets/left_bar.dart';
import 'package:imc_aula/widgets/rigth_bar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  TextEditingController _alturaController = TextEditingController();
  TextEditingController _pesoController = TextEditingController();

  double _imcResult = 0;
  String _textResult = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Calcular IMC",
          style: TextStyle(color: accentHexacolor, fontWeight: FontWeight.w300),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: mainHexcolor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 130,
                  child: TextField(
                    controller: _alturaController,
                    style: TextStyle(
                        fontSize: 42,
                        fontWeight: FontWeight.w300,
                        color: accentHexacolor),
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "ALTURA",
                        hintStyle: TextStyle(
                            fontSize: 32,
                            fontWeight: FontWeight.w300,
                            color: Colors.white.withOpacity(0.8))),
                  ),
                ),
                Container(
                  width: 130,
                  child: TextField(
                    controller: _pesoController,
                    style: TextStyle(
                        fontSize: 42,
                        fontWeight: FontWeight.w300,
                        color: accentHexacolor),
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "PESO",
                        hintStyle: TextStyle(
                            fontSize: 32,
                            fontWeight: FontWeight.w300,
                            color: Colors.white.withOpacity(0.8))),
                  ),
                )
              ],
            ),
            SizedBox(height: 30),
            GestureDetector(
              onTap: (){

                double _altura = double.parse(_alturaController.text);
                double _peso = double.parse(_pesoController.text);

                setState(() {

                  _imcResult = _peso / (_altura * _altura);

                  if(_imcResult > 25){
                    _textResult = "Você esta acima do PESO";
                  }else if(_imcResult >= 18.5 && _imcResult <= 25){
                    _textResult = "Você esta no peso NORMAL";
                  }else{
                    _textResult = "Você está abaixo do PESO";
                  }

                });
              },
              child: Container(
                child: Text(
                  "CALCULAR",
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: accentHexacolor),
                ),
              ),
            ),
            SizedBox(height: 50),
            Container(
              child: Text(
                _imcResult.toStringAsFixed(2),
                style: TextStyle(fontSize: 90, color: accentHexacolor),
              ),
            ),
            SizedBox(height: 30),
            Visibility(
              child: Container(
                child: Text(
                    _textResult,
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.w400,
                    color: accentHexacolor
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            LeftBar(barWidth: 40),
            SizedBox(height: 20),
            LeftBar(barWidth: 70),
            SizedBox(height: 20),
            LeftBar(barWidth: 40),
            SizedBox(height: 10),
            RigthBar(barWidth: 70),
            SizedBox(height: 40),
            RigthBar(barWidth: 60),
          ],
        ),
      ),
    );
  }
}
